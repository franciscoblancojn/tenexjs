import { useRef, useState } from "react";
import * as Yup from "yup";
import { AnySchema } from "yup";
import styled from "styled-components";

import { logError } from "~/functions/log.c";

export type InputStatus = "" | "ok" | "error" | "warning";
export type InputType = "text" | "email" | "password" | "date";

export interface InputResultValidate {
    type: InputStatus;
    message: string;
    value: string;
}
export interface InputTextProps {
    yup: AnySchema;
    type?: InputType;
    label?: string;
    placeholder?: string;
    defaultValue?: string;
    value?: string | null;
    onChange: (value: string) => void;
    onBlur: (value: string) => void;
    onChangeValueBeforeYup: (value: string) => Promise<string> | string;
    onChangeValidateBeforeYup: (
        value: string
    ) => Promise<InputResultValidate> | InputResultValidate;
    onChangeValidateAfterYup: (
        value: string
    ) => Promise<InputResultValidate> | InputResultValidate;
    icon: any;
}

export const InputStyle = styled.div`
    .TENEXJS {
        &-input-content-label {
        }
        &-input-label {
            display: block;
        }
        &-input {
        }
        &-input-icon {
        }
        &-input-error {
        }
    }
`;
export const InputText = ({
    yup = Yup.string(),
    type = "text",
    label = "",
    placeholder = "",
    defaultValue = "",
    value = null,
    onChange = (value: string) => {
        return;
    },
    onBlur = (value: string) => {
        return;
    },
    onChangeValueBeforeYup = async (value: string) => value,
    onChangeValidateBeforeYup = async (value: string) => ({
        type: "ok",
        message: "",
        value,
    }),
    onChangeValidateAfterYup = async (value: string) => ({
        type: "ok",
        message: "",
        value,
    }),
    icon = <></>,
}: InputTextProps) => {
    const [statusInput, setStateInput] = useState<InputStatus>("");
    const [error, setError] = useState<string>("");
    const [valueInput, setValueInput] = useState<string>(defaultValue);
    const ref = useRef<any>(null);

    const validateValue = async (v: string) => {
        try {
            v = await onChangeValueBeforeYup(v);
            const resultValidateBefore = await onChangeValidateBeforeYup(v);
            if (resultValidateBefore.type == "error") {
                throw resultValidateBefore;
            }
        } catch (error: any) {
            logError({
                name: "error validate input value",
                data: error,
            });
            setStateInput("error");
            setError(error.message);
            return v;
        }
        yup.validate(v)
            .then(async function (valid) {
                if (valid) {
                    setStateInput("ok");
                    setError("");
                    try {
                        const resultValidateAfter =
                            await onChangeValidateAfterYup(v);
                        if (resultValidateAfter.type == "error") {
                            throw resultValidateAfter;
                        }
                    } catch (error: any) {
                        logError({
                            name: "error validate input value",
                            data: error,
                        });
                        setStateInput("error");
                        setError(error.message);
                        return;
                    }
                }
            })
            .catch(function (error) {
                logError({
                    name: "error validate input value",
                    data: error,
                });
                setStateInput("error");
                setError(error.message);
                return;
            });
        return v;
    };
    const changeInput = async () => {
        const input = ref.current;
        const pointer = input.selectionStart;

        const v = await validateValue(input.target.value);
        setValueInput(v);
        onChange(v);
        try {
            input.setSelectionRange(pointer, pointer);
        } catch (error) {
            return;
        }
    };
    const blurInput = () => {
        validateValue(valueInput);
        onBlur(valueInput);
    };

    return (
        <InputStyle>
            <label className={`TENEXJS-input-content-label`}>
                {label != "" && (
                    <div className={`TENEXJS-input-label`}>{label}</div>
                )}
                <div className={`TENEXJS-input-content-input`}>
                    <input
                        type={type}
                        ref={ref}
                        className={`TENEXJS-input ${statusInput}`}
                        placeholder={placeholder}
                        value={value ?? valueInput}
                        onChange={changeInput}
                        onBlur={blurInput}
                    />
                    <span className={`TENEXJS-input-icon`}>{icon}</span>
                </div>
                {error != "" && (
                    <div className={`TENEXJS-input-error`}>{error}</div>
                )}
            </label>
        </InputStyle>
    );
};
