import { logError } from "~/functions/log.c";

export interface moneyProps {
    price: string | number;
    style?: string;
    currency?: string;
    minimumFractionDigits?: number;
    maximumFractionDigits?: number;
}

export const money = (props: moneyProps) => {
    try {
        const price = `${props.price}`;
        const priceN = parseFloat(price.replace(/[*+?^${}()|[\]\\]/g, ""));
        if (Number.isNaN(priceN)) {
            return null;
        }
        const options = {
            style: "currency",
            currency: "USD",
            minimumFractionDigits: 0,
            maximumFractionDigits: 2,
            ...props,
        };
        const numberFormat = new Intl.NumberFormat("en-US", options);
        return numberFormat.format(priceN);
    } catch (error) {
        logError({
            name: "Error in converte money",
            data: error,
        });
        return "";
    }
};
