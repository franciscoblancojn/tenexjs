export interface logDataProps {
    name: string;
    data: any;
}
export interface logProps extends logDataProps {
    type?: "error" | "log" | "table" | "warn";
    color?: string;
}

export const log = (props: logProps) => {
    const { name, data, type } = props;
    const color = props?.color ?? "gray";

    if (process?.env?.MODE === "DEV") {
        console[type ?? "log"](
            `%c [${name.toLocaleUpperCase()}]`,
            `color:${color};`,
            data
        );
    }
};

export const logError = (props: logDataProps) => {
    log({
        color: "red",
        type: "error",
        ...props,
    });
};
