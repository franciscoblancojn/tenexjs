import { useState, useEffect } from "react";
import type { AnySchema } from "yup";

export interface useValidationProps {
    value: any;
    parceError?: (error: any) => string;
    schema: AnySchema;
}

export const useValidation = (props: useValidationProps) => {
    const [error, setError] = useState<string | null>(null);

    const onValidation = () => {
        props.schema
            .validate(props.value)
            .then(() => {
                setError(null);
            })
            .catch((e) => {
                const error = props?.parceError?.(e) ?? e;
                setError(error);
            });
    };
    useEffect(() => {
        onValidation();
    }, [props.value]);
    return {
        isValid: error == null,
        isError: error != null,
        error,
    };
};
