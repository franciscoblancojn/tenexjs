import replace from "replace-in-file";

const results = replace.sync({
    files: "dist/**/*.js",
    from: /from(.){0,100}.c";/g,
    to: (input) => {
        const n = input.replace('";', '.js";');
        console.log(`Replace [${input}] form [${n}]`);
        return n;
    },
});

console.log(results);
